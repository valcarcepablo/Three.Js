import * as THREE from "/node_modules/three/build/three.module.js";
import { GLTFLoader } from '/node_modules/three/examples/jsm/loaders/GLTFLoader'
import { OrbitControls } from '/node_modules/three/examples/jsm/controls/OrbitControls'
import CannonDebugger from '/node_modules/cannon-es-debugger'
import * as CANNON from '/node_modules/cannon-es'
import { gsap } from "/node_modules/gsap";

const world = new CANNON.World();
const clock = new THREE.Clock();
const scene = new THREE.Scene();
const camera = new THREE.PerspectiveCamera( 80, innerWidth / innerHeight, 1, 2000 );
const renderer = new THREE.WebGLRenderer();
renderer.setSize( innerWidth, innerHeight );
document.body.appendChild( renderer.domElement );
world.gravity.set(0, -9.81, 0);
const cannonDebugger = new CannonDebugger(scene, world);
let car;
let filesToLoad = 4;
const orbitControls = new OrbitControls(camera, renderer.domElement);
orbitControls.enableDamping = true
orbitControls.minDistance = 5
orbitControls.maxDistance = 15
orbitControls.enablePan = true
orbitControls.maxPolarAngle = Math.PI / 2 - 0.05
orbitControls.update();

scene.background = new THREE.Color( 0xa0a0a0 );
scene.fog = new THREE.Fog( 0xe0e0e0, 20, 100 );
camera.position.set(-6,2,-9);
camera.lookAt(0,1,-5);

const hemiLight = new THREE.HemisphereLight( 0xffffff, 0x444444 );
hemiLight.position.set( 0, 20, 0 );
scene.add( hemiLight );


const dirLight = new THREE.DirectionalLight( 0xffffff );
dirLight.position.set( - 3, 10, - 10 );
dirLight.castShadow = true;
dirLight.shadow.camera.top = 2;
dirLight.shadow.camera.bottom = - 2;
dirLight.shadow.camera.left = - 2;
dirLight.shadow.camera.right = 2;
dirLight.shadow.camera.near = 0.1;
dirLight.shadow.camera.far = 40;
scene.add( dirLight );

const _VS = `
uniform float pointMultiplier;
attribute float size;
attribute float angle;
attribute vec4 colour;
varying vec4 vColour;
varying vec2 vAngle;
void main() {
  vec4 mvPosition = modelViewMatrix * vec4(position, 1.0);
  gl_Position = projectionMatrix * mvPosition;
  gl_PointSize = size * pointMultiplier / gl_Position.w;
  vAngle = vec2(cos(angle), sin(angle));
  vColour = colour;
}`;

const _FS = `
uniform sampler2D diffuseTexture;
varying vec4 vColour;
varying vec2 vAngle;
void main() {
  vec2 coords = (gl_PointCoord - 0.5) * mat2(vAngle.x, vAngle.y, -vAngle.y, vAngle.x) + 0.5;
  gl_FragColor = texture2D(diffuseTexture, coords) * vColour;
}`;

class LinearSpline {
  constructor(lerp) {
    this._points = [];
    this._lerp = lerp;
  }

  AddPoint(t, d) {
    this._points.push([t, d]);
  }

  Get(t) {
    let p1 = 0;

    for (let i = 0; i < this._points.length; i++) {
      if (this._points[i][0] >= t) {
        break;
      }
      p1 = i;
    }

    const p2 = Math.min(this._points.length - 1, p1 + 1);

    if (p1 == p2) {
      return this._points[p1][1];
    }

    return this._lerp(
        (t - this._points[p1][0]) / (
            this._points[p2][0] - this._points[p1][0]),
        this._points[p1][1], this._points[p2][1]);
  }
}

class ParticleSystem {
  constructor(params) {
   console.log(params)
    const uniforms = {
        diffuseTexture: {
            value: new THREE.TextureLoader().load(params.sprite)
          
        },
        pointMultiplier: {
            value: window.innerHeight / (2.0 * Math.tan(0.5 * 60.0 * Math.PI / 180.0))
        }
    };

    this._material = new THREE.ShaderMaterial({
        uniforms: uniforms,
        vertexShader: _VS,
        fragmentShader: _FS,
        blending: THREE.AdditiveBlending,
        depthTest: true,
        depthWrite: false,
        transparent: true,
        vertexColors: true
    });
    
    this._camera = params.camera;
    this._particles = [];
    this.particleObject = params
    
    this._geometry = new THREE.BufferGeometry();
    this._geometry.setAttribute('position', new THREE.Float32BufferAttribute([], 3));
    this._geometry.setAttribute('size', new THREE.Float32BufferAttribute([], 1));
    this._geometry.setAttribute('colour', new THREE.Float32BufferAttribute([], 4));
    this._geometry.setAttribute('angle', new THREE.Float32BufferAttribute([], 1));

    this._points = new THREE.Points(this._geometry, this._material);
    params.parent.add(this._points);

    this._alphaSpline = new LinearSpline((t, a, b) => {
      return a + t * (b - a);
    });
    this._alphaSpline.AddPoint(0.0, 0.0);
    this._alphaSpline.AddPoint(0.1, 1.0);
    this._alphaSpline.AddPoint(0.6, 1.0);
    this._alphaSpline.AddPoint(1.0, 0.0);

    this._colourSpline = new LinearSpline((t, a, b) => {
      const c = a.clone();
      return c.lerp(b, t);
    });
    this._colourSpline.AddPoint(0.0, new THREE.Color(0xAAFF80));
    this._colourSpline.AddPoint(1.0, new THREE.Color(0xAA8080));

    this._sizeSpline = new LinearSpline((t, a, b) => {
      return a + t * (b - a);
    });
    this._sizeSpline.AddPoint(0.0, 1.0);
    this._sizeSpline.AddPoint(0.5, 3.0);
    this._sizeSpline.AddPoint(1.0, 1.0);

   
   
    this._UpdateGeometry();
  }
  
  _AddParticles(delta) {
    if (!this.gdfsghk) {
      this.gdfsghk = 0.0;
    }
   
    this.gdfsghk += delta;
    const n = Math.floor(this.gdfsghk * 75.0);
    this.gdfsghk -= n / 60.0;
    
    const positionParticle = new THREE.Vector3(this.particleObject.particlePosition.x,this.particleObject.particlePosition.y,this.particleObject.particlePosition.z) 
    const speedParticule = new THREE.Vector3(this.particleObject.speed.x,this.particleObject.speed.y,this.particleObject.speed.z)
    for (let i = 0; i < n; i++) {
     
      this._particles.push({
          position: positionParticle,
          size: Math.random() * this.particleObject.size,
          colour: new THREE.Color(),
          alpha:Math.random() * this.particleObject.alpha+1,
          life: Math.random() * this.particleObject.life + 1,
          maxLife: Math.random() * this.particleObject.life+1,
          rotation: Math.random() * this.particleObject.rotation+1,
          velocity: speedParticule,
      });
    }
  }

  _UpdateGeometry() {
    const positions = [];
    const sizes = [];
    const colours = [];
    const angles = [];

    for (let p of this._particles) {
      positions.push(p.position.x, p.position.y, p.position.z);
      colours.push(p.colour.r, p.colour.g, p.colour.b, p.alpha);
      sizes.push(p.currentSize);
      angles.push(p.rotation);
    }

    this._geometry.setAttribute(
        'position', new THREE.Float32BufferAttribute(positions, 3));
    this._geometry.setAttribute(
        'size', new THREE.Float32BufferAttribute(sizes, 1));
    this._geometry.setAttribute(
        'colour', new THREE.Float32BufferAttribute(colours, 4));
    this._geometry.setAttribute(
        'angle', new THREE.Float32BufferAttribute(angles, 1));
  
    this._geometry.attributes.position.needsUpdate = true;
    this._geometry.attributes.size.needsUpdate = true;
    this._geometry.attributes.colour.needsUpdate = true;
    this._geometry.attributes.angle.needsUpdate = true;
  }

  _UpdateParticles(delta) {
    for (let p of this._particles) {
      p.life -= delta;
    }

    this._particles = this._particles.filter(p => {
      return p.life > 0.0;
    });

    for (let p of this._particles) {
      const t = 1.0 - p.life / p.maxLife;

      p.rotation += delta * 0.5;
      p.alpha = this._alphaSpline.Get(t);
      p.currentSize = p.size * this._sizeSpline.Get(t);
      p.colour.copy(this._colourSpline.Get(t));

      p.position.add(p.velocity.clone().multiplyScalar(delta));

      const drag = p.velocity.clone();
      drag.multiplyScalar(delta * 0.1);
      drag.x = Math.sign(p.velocity.x) * Math.min(Math.abs(drag.x), Math.abs(p.velocity.x));
      drag.y = Math.sign(p.velocity.y) * Math.min(Math.abs(drag.y), Math.abs(p.velocity.y));
      drag.z = Math.sign(p.velocity.z) * Math.min(Math.abs(drag.z), Math.abs(p.velocity.z));
      p.velocity.sub(drag);
    }



    this._particles.sort((a, b) => {
      const d1 = this._camera.position.distanceTo(a.position);
      const d2 = this._camera.position.distanceTo(b.position);

      if (d1 > d2) {
        return -1;
      }

      if (d1 < d2) {
        return 1;
      }

      return 0;
    });
  }

  Step(delta) {
    fire._AddParticles(delta);
    this._UpdateParticles(delta);
    this._UpdateGeometry();
   
    
  }
}


let fire = new ParticleSystem({
  parent: scene,
  camera: camera,
  sprite: 'assets/animations/fire.png',
  particlePosition: new THREE.Vector3(0,0.5,0),
  size: 0.4,
  alpha:1,
  rotation: Math.random() * 2.0 * Math.PI,
  speed: new THREE.Vector3(0,0.7,0),
  life: 1,
})



const normalMaterial = new THREE.MeshNormalMaterial()
let maxForce
let model;

const planeShape = new CANNON.Plane()
const wallShape = new CANNON.Box(new CANNON.Vec3(15,5,0.1))
const wallShape2 = new CANNON.Box(new CANNON.Vec3(0.1,5,15))
const planeBody = new CANNON.Body({ type:CANNON.Body.STATIC })
const wall1 = new CANNON.Body({ type:CANNON.Body.STATIC })
const wall2 = new CANNON.Body({ type:CANNON.Body.STATIC })
const wall3 = new CANNON.Body({ type:CANNON.Body.STATIC })
const wall4 = new CANNON.Body({ type:CANNON.Body.STATIC })
wall1.addShape(wallShape)
wall2.addShape(wallShape)
wall3.addShape(wallShape2)
wall4.addShape(wallShape2)


world.addBody(wall1)
world.addBody(wall2)
world.addBody(wall3)
world.addBody(wall4)
wall1.position.z = 14
wall2.position.z = -14
wall3.position.x = 14
wall4.position.x = -14

planeBody.addShape(planeShape)
planeBody.quaternion.setFromAxisAngle(new CANNON.Vec3(1, 0, 0), -Math.PI / 2)
world.addBody(planeBody)
const loader = new GLTFLoader();

loader.load('./assets/scenario/scene.glb', function (gltf ) {

  const model = gltf.scene;

  model.traverse( function ( object ) {

    if ( object.isMesh ) object.castShadow = true;

  } );
  scene.add( model );
  filesToLoad--;
}
)

loader.load('/assets/character/character_mage.gltf', function(gltf){

  model = gltf.scene
  model.position.set(-4,0,-7)
  model.scale.set(0.8,0.8,0.8)
  model.rotation.y = 4
  scene.add(model)
 
  filesToLoad--;

}
)


function jumpAnimation(){
  console.log(model.scale)
  if(model.position.y === 0 && model.scale.y === 0.8){
    gsap.to(model.scale, { duration:0.2, y:0.5 , yoyo:true , repeat:1,  ease:'power4.out'}) // se prepara para saltar
    gsap.to(model.position,{delay:0.4,duration: 0.4, y: 1, ease:'power2.out'}) //salta
    gsap.to(model.position,{delay:0.85,duration: 0.4, y: 0, ease:'power2.in'}) // cae
    gsap.to(model.scale, {delay:1.25, duration:0.13, y:0.6, yoyo:true, repeat:1, ease:'power2 .out'}) // bounce al caer
  }
}

loader.load('./scene.gltf', function (gltf ) {

  car = gltf.scene;
  car.scale.set(0.4,0.4,0.4)
  car.position.set(-2,0,-2)
  car.rotation.y += 4.8
  scene.add( car );
  filesToLoad--;
}
)
loader.load('assets/scenario/staff_rare.gltf.glb', function (gltf ) {

  const staff = gltf.scene;
  scene.add( staff );
  filesToLoad--;
}
)

const chassisShape = new CANNON.Box(new CANNON.Vec3(1, 1, 1,8))
const chassisBody = new CANNON.Body({ mass: 2})
const centerOfMassAdjust = new CANNON.Vec3(0, 0, 0)
chassisBody.addShape(chassisShape, centerOfMassAdjust)
chassisBody.position.set(-2,3.2,-2)
world.addBody(chassisBody)

const vehicle = new CANNON.RigidVehicle({
  chassisBody,
})

const mass = 1
const axisWidth = 7
const wheelShape = new CANNON.Sphere(0.4)
const wheelMaterial = new CANNON.Material('wheel')
const down = new CANNON.Vec3(0, -1, 0)

const wheelBody1 = new CANNON.Body({ mass, material: wheelMaterial })
wheelBody1.addShape(wheelShape)
vehicle.addWheel({
  body: wheelBody1,
  position: new CANNON.Vec3(-1, -1, 1).vadd(centerOfMassAdjust),
  axis: new CANNON.Vec3(1, 0, 0),
  direction: down,
  friction: 10
})

const wheelBody2 = new CANNON.Body({ mass, material: wheelMaterial })
wheelBody2.addShape(wheelShape)
vehicle.addWheel({
  body: wheelBody2,
  position: new CANNON.Vec3(-1, -1, -1).vadd(centerOfMassAdjust),
  axis: new CANNON.Vec3(-1, 0, 0),
  direction: down,
  friction: 10
})

const wheelBody3 = new CANNON.Body({ mass, material: wheelMaterial })
wheelBody3.addShape(wheelShape)
vehicle.addWheel({
  body: wheelBody3,
  position: new CANNON.Vec3(1, -1, 1).vadd(centerOfMassAdjust),
  axis: new CANNON.Vec3(1, 0, 0),
  direction: down,
  friction: 10
})

const wheelBody4 = new CANNON.Body({ mass, material: wheelMaterial })
wheelBody4.addShape(wheelShape)
vehicle.addWheel({
  body: wheelBody4,
  position: new CANNON.Vec3(1, -1, -1).vadd(centerOfMassAdjust),
  axis: new CANNON.Vec3(-1, 0, 0),
  direction: down,
  friction: 10
})


vehicle.wheelBodies.forEach((wheelBody) => {
  
  wheelBody.angularDamping = 0.2
 
})


const wheel_ground = new CANNON.ContactMaterial(wheelMaterial, normalMaterial, {
  friction:10,
  restitution: 0,
  contactEquationStiffness: 1000,
})
world.addContactMaterial(wheel_ground)

vehicle.addToWorld(world)



document.addEventListener('keydown', (event) => {
  const maxSteerVal = Math.PI / 5
  maxForce = 15

  switch (event.key) {
    case " " :
    jumpAnimation();
   
      break

    case 'w':
    case 'ArrowUp':
    
      vehicle.setWheelForce(-maxForce / 2, 1)
      vehicle.setWheelForce(-maxForce / 2, 3)
      break

    case 's':
    case 'ArrowDown':
      vehicle.setWheelForce(maxForce / 2, 1)
      vehicle.setWheelForce(maxForce / 2, 3)
      break

    case 'a':
    case 'ArrowLeft':
     
      vehicle.setSteeringValue(maxSteerVal, 0)
      vehicle.setSteeringValue(maxSteerVal, 2)
  
      break

    case 'd':
    case 'ArrowRight':
    
      vehicle.setSteeringValue(-maxSteerVal, 0)
      vehicle.setSteeringValue(-maxSteerVal, 2)
      break
  }
})

document.addEventListener('keyup', (event) => {
  switch (event.key) {
    
    case 'w':
    case 'ArrowUp':
     
    vehicle.setWheelForce(0,1)
    vehicle.setWheelForce(0,3)
   

      break

    case 's':
    case 'ArrowDown':
      vehicle.setWheelForce(0, 1)
      vehicle.setWheelForce(0, 3)

     
      break

    case 'a':
    case 'ArrowLeft':
      vehicle.setSteeringValue(0, 0)
      vehicle.setSteeringValue(0, 2)
     
      break

    case 'd':
    case 'ArrowRight':
      vehicle.setSteeringValue(0, 0)
      vehicle.setSteeringValue(0, 2)
    
      break
  }
})


world.broadphase = new CANNON.SAPBroadphase(world)


animate(); 

function update() {


  
  car.position.x = vehicle.chassisBody.position.x
  car.position.z = vehicle.chassisBody.position.z
  car.quaternion.z = vehicle.chassisBody.quaternion.z
  car.quaternion.x = vehicle.chassisBody.quaternion.x
  car.quaternion.y = vehicle.chassisBody.quaternion.y
  car.quaternion.w= vehicle.chassisBody.quaternion.w

 

 if (maxForce> 30) maxForce = 30
  
}	



function animate() {
 // update()
 cannonDebugger.update()

  requestAnimationFrame( animate );
  
  const delta = clock.getDelta();
  fire.Step(delta,fire.particleObject)
 
  world.step(delta)
  if(filesToLoad === 0){
    update()

  }
  
 
  renderer.render( scene, camera );
}
